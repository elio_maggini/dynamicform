# Dynamic Form Demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

It is sample code showing a dynamic form generated from JSON metadata. This is a simplified version of previous work I have done.

Stripped out of this example are call to backend server for dynamic JSON, file upload, and form posting.

It uses Angular Material in a mobile-first, responsive design fashion.

If anyone beyond the job interview discovers this code and finds it useful, please let me know if you would like it extended and I will work on it.

Dear job people.....if I have time before you look at this I will add some meaningful tests. Right now it is all boiler plate.
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
