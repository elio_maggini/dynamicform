import {Component, OnInit} from '@angular/core';

import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  displayValue: string;
  form: FormGroup;
  objectProps;

  // normally this would come from an HTTP call. Hardcoded since this example is about dynamic forms
  dataObject = {
    'telephone': {
      'type': 'telephone',
      'label': 'Enter telephone',
      'validation': {'required': true, 'max': '9999999999', 'min': '2012000000'}
    },
    'date': {'type': 'date', 'label': 'Enter date', 'validation': {'required': true}},
    'rebate': {
      'id': 'rebates_2017',
      'ref_id': 'subcategory_2017',
      'type': 'select',
      'label': 'Rebate',
      'options': [{
        'label': '$10 Rebate on 4-Pack of Bulbs',
        'value': '$10 Rebate on 4-Pack of Bulbs'
      }, {
        'label': '$20 Rebate on 8-Pack of Bulbs',
        'value': '$20 Rebate on 8-Pack of Bulbs'
      }]
    },
    'item': {
      'id': 'subcategory_2017', 'type': 'select', 'label': 'Items(s) purchased', 'options': [
        {'label': 'SuperCorp SKU ABC-123a', 'value': 'SuperCorp SKU ABC-123a'},
        {'label': 'MegaCorp SKU C-1323a', 'value': 'MegaCorp SKU C-1323a'}
      ]
    },
    'quantity_purchase': {'label': 'Quantity purchased:', 'type': 'number', 'validation': {'required': true, 'max': '3', 'min': '1'}},
    'wattage': {
      'validation': {'required': true, 'max': '100', 'min': '1'},
      'type': 'number',
      'label': 'What wattage bulb will they replace'
    }
  };


  ngOnInit() {

    // this maps data from JSON structure to formGroup structure
    this.objectProps =
      Object.keys(this.dataObject)
        .map(prop => {
          return Object.assign({}, {key: prop}, this.dataObject[prop]);
        });

    // setup the form
    const formGroup = {};
    for (const prop of Object.keys(this.dataObject)) {
      formGroup[prop] = new FormControl(this.dataObject[prop].value || '', this.mapValidators(this.dataObject[prop].validation));
    }

    this.form = new FormGroup(formGroup);

  }

  private dataIsComplete() {
    return this.form.valid;
  }

  private mapValidators(validators) {
    const formValidators = [];

    // here we can add custom validators
    if (validators) {
      for (const validation of Object.keys(validators)) {
        if (validation === 'required') {
          formValidators.push(Validators.required);
        } else if (validation === 'max') {
          formValidators.push(Validators.max(validators[validation]));
        } else if (validation === 'min') {
          formValidators.push(Validators.min(validators[validation]));
        }
      }
    }
    return formValidators;
  }

  private submitApplication() {
    this.displayValue = this.form.getRawValue();
  }
}
